/* 
 * File:   main.cpp
 * Author: David Ramirez
 * this code is my work
 * Created on March 17, 2015, 5:19 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int number_of_people;
    int safe_number = 70;
    
    cout << " Enter the number of occupants:";

    cin >> number_of_people;

    if (number_of_people > safe_number) 
    {
        cout << " Over capacity!!! ";
    } 
    else if (number_of_people <= safe_number) 
    {
        cout << " We are within capacity. Carry on.";
    }

    return 0;
}

