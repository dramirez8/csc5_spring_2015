/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2015, 12:13 PM
 */

#include <cstdlib>
#include <fstream>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    // step 1 open he file
    // ofstream is the data type
    ofstream outfile;
    outfile.open("test.txt");
    
    // while loop to allow continues input
    cout << "Enter a number. -1 to quit";
    int num;
    cin >> num;
    while ( num != -1)
    {
        // write to file STEP 2
        outfile << num << endl;
        
        cout << " Enter a number. -1 to quit. ";
        cin >> num;
    }
    
    // STEP 3 close file
    outfile.close();
        
        // read abd sum all values in a file
    
    ifstream infile;
    infile.open("test.txt");
    // check if file exist
    if (!infile)
    {
        cout << " Not a valid file! ";
    }
            else
            {
                // Sum variable
                int sum = 0;
            
            // input
            int num;
            while ( infile >> num)
            {
                sum += num;
                
     
         }
            cout << " Your sum is :"
                    << sum < endl;
        }
    // 
    infile/close();
    
    return 0;
