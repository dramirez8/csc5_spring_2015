/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 10, 2015, 11:22 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // promt the use.
    cout << " Please enter a number" << endl;
    cout << "Enter 1 for Apples" << endl
            << "Enter 2 for Oranges" << endl
            << "Enter 3 for Grapes";
    // User input
    int num;
    cin >> num;

    switch (num) 
    {
        case 1:
            cout << "Your earned Apples ";
        case 2:
            cout << " You earned oranges ";
        case 3:
            cout << " You earned Grapes ";
            break;
        default:
            cout << " No fruit earned";

    }
    cout << endl;

    // string member functions
    string userInput;
    cin >> userInput;

    cout << "You entered: " << userInput.size()
            << " characters.";
    return 0;

   
}

