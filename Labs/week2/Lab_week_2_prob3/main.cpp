/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 7, 2015, 11:50 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    int feetpermeter;
    float meterspermile;
    float inchperfeet;
    float meter;
    //int mile;
    
    
    cout << "Please enter meter\n";
    cin >> meter;
    meterspermile = meter / 1609.344;
    feetpermeter = meter * 3.281;
    inchperfeet = 12 / meter * 3.281;
    cout << "miles: " << meterspermile << "\nfeet: " << 
            feetpermeter <<"\nInches: "<< inchperfeet <<  endl; 
    
    return 0;
}

