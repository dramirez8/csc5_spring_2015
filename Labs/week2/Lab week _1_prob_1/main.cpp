/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 7, 2015, 11:42 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    string name;
    cout << "Hello, my name is Hal!\n"
          << "What is your name?\n";
    cin >> name;
    cout << "Hello, " << name << ". I am glad to meet you.";

    return 0;
}

