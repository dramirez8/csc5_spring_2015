/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2015, 10:49 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int main(int argc, char** argv) {
    // get a number from he user.
    // Promt the user
    cout << "Please use a number: ";
    
    int num;
    cin >> num;
    // Heck if positive or negative.
    if (num >= 0)
    {
        cout << " Positive number!";
    }
    else
    {
        cout << "Negative Number!";
    }
    cout << endl;
    
    //CHeck if even or odd
    int rem = num % 2;
    // remainder is either 0 or 1
    if (num % 2 == 0)
    {
        cout << "Even number!";
    }
    else
    {
        cout << "Odd number!";
    
    }
    return 0;
}

