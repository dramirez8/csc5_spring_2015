/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 28, 2015, 11:39 AM
 */

#include <cstdlib>
#include <vector>
#include <iostream>

using namespace std;

/*
 * 
 */
void output(vector <int>& v)
    {
        for (int i = 0; i < v.size(); i++) 
        {
            cout << v[i] << endl;
        }
    }
int main(int argc, char** argv) {

    vector<int> v;

    v.push_back(3);
    v.push_back(6);
    v.push_back(8);
    
    output(v);// you can't use "cout" to output a vector you just have to call the
                // function.
    





    return 0;
}
