/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 5, 2015, 11:09 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

int main(int argc, char** argv) {


    int x = 9;
    int y = 0;
    
    if (x == 4) {
        y = 4;
    } else if (x == 9) {
        y = 5;
    } else {
        y = 6;
    }
    cout << "x=" << x << endl << "y=" << y;
    return 0;
}
