/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2015, 11:48 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string text = "Hello my name is Hal!\n";
    cout << text;
    string quest = "what is your name? \n";
    cout << quest;
    string name;
    cin >> name;
    cout << "Hello, ";
    cout << name;
    cout << ". I'm glad to meet you!";

    return 0;
}

