/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 24, 2015, 10:36 AM
 */

#include <cstdlib>
#include <fstream> // file IO
#include <iostream>
using namespace std;

int main(int argc, char** argv) {

    // STEP 1 Open the file
    // ofstream is the data type
    ofstream outfile;
    outfile.open("test.txt");
    
    // While loop to allow continous input
    cout << "Enter a number. -1 to quit. ";
    int num;
    cin >> num;
    
    while (num != -1)
    {
        // Write to file STEP 2
        outfile << num << endl;
        
        cout << "Enter a number. -1 to quit. ";
        cin >> num;
    }
    
    // Step 3 CLOSE FILE
    outfile.close();
    
    // Read and sum all values in a file
    ifstream infile;
    infile.open("test.txt");
    
    // Check if file exists
    if (!infile)
    {
        cout << "Not a valid file!";
    }
    else
    {
        // Sum variable
        int sum = 0;
        
        // Input
        int num;
        while(infile >> num)
        {
            sum += num;
        }
        
        outfile.open("sum.txt");
        cout << "Your sum is: " << sum << endl;
        outfile << "Your sum is: " << sum << endl;
        outfile.close();
    }
    
    // Close
    infile.close();
    return 0;
}