/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2015, 10:41 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Use a while loop for user controlled loops
    // Prompt first
    srand(time(0)); // Random num
    
    cout << "Do you want to play a game? yes/no: ";
    string answer;
    cin >> answer;
    
    // Get random num
    int randNum = rand() % 100;
    
    while (answer == "yes")
    {
        cout << "Please enter a number: " << endl;
        int num;
        cin >> num;
        
        // Random number
        if (num > randNum)
        {
            cout << "Too high";
        }
        else if (num < randNum)
        {
            cout << "Too low"; 
        }
        else
        {
            cout << "Correct!";
        }
        
        cout << endl;
        
        // Prompt again for loop
        cout << "Do you want to play again? yes/no: ";
        cin >> answer;
    }
    
    return 0;
}