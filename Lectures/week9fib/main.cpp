/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 21, 2015, 10:45 AM
 */

#include <cstdlib>
#include <iostream>
// Step 1: Describe function
// Step 2: Describe parameters
// Step 3: Describe return value

/**
 * The fibb function calculates the
 * fibonacci sequence. Starting values
 * are 0 and 1.
 * @param num The desired sequence
 * @return Return the value at the 
 * wanted sequence
 */

using namespace std;
int fibb(int num)
{
    cout << "Calculating fibb: " << num << endl;
  //Base case
    if(num ==2)
        return 1;
    else if(num ==1)
        return 0;
    else // Recursive call
        return fibb(num -1)+fibb(num -2);
}
/*
 * 
 */
int main(int argc, char** argv) 
{
    int num;
    cout << " Please enter a number : ";
    cin >> num;
    cout << " Fib of: " << num << " is "
            << fibb(num);
    return 0;
    
}

